$(document).ready(function (){

    //initialize carousel
    $('.carousel').carousel();

   //scroll tracker


        // if(scrolled>1){
        //     $('html, body').stop().animate({
        //         'scrollTop': $target.offset().top-60
        //     }, 1000, 'swing', function () {
        //         window.location.hash = target;
        //         $(document).on("scroll", onScroll);
        //     });
        // }


    //smooth scroll
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 1000, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });

//     var options = [
//        {selector: '#individual', offset: 300, callback: function(el)
//        { Materialize.fadeInImage($(el)); } },
//        {selector: '#goods', offset: 300, callback: function(el)
//        { Materialize.fadeInImage($(el)); } },
//        {selector: '#bespoke', offset: 300, callback: function(el)
//        { Materialize.fadeInImage($(el)); } },
//        {selector: '#materials', offset: 300, callback: function(el)
//        { Materialize.fadeInImage($(el)); } },
//        {selector: '#photos', offset: 300, callback: function(el)
//        { Materialize.fadeInImage($(el)); } },
//        {selector: '#form', offset: 300, callback: function(el)
//        { Materialize.fadeInImage($(el)); } },
//    ];
//
//    Materialize.scrollFire(options);
});
//$(window).scroll(
//  {
//    previousTop: 0
//  },
//  function () {
//    var currentTop = $(window).scrollTop();
//    if (currentTop < this.previousTop) {
//      $(".sidebar em").text("Up"); /* optional for demo */
//      $(".header").show();
//    } else {
//      $(".sidebar em").text("Down");
//      $(".header").hide();
//    }
//    this.previousTop = currentTop;
//  });

$(window).scroll(
  {
    previousTop: 0
  },
  function () {
    var currentTop = $(window).scrollTop();
    if (currentTop < this.previousTop) {
//      $("header.main").show();
      $("header.main").slideDown("slow");
    } else {
//      $("header.main").hide();
      $("header.main").slideUp("slow");
    }
    this.previousTop = currentTop;
  });

$( ".map-filter" ).click(function() {
  $( ".map-filter" ).remove();
});
